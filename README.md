# ![XSX-Logo](doc/xsx-logo.png?raw=true) – eXtended S-eXpressions

[![Test Coverage](https://img.shields.io/badge/coverage-86%25-yellow.svg)](file:coverage.html)
[![Go Report Card](https://goreportcard.com/badge/codeberg.org/fractalqb/xsx)](https://goreportcard.com/report/codeberg.org/fractalqb/xsx)
[![Go Reference](https://pkg.go.dev/badge/git.fractalqb.de/fractalqb/xsx.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/xsx)

`import "git.fractalqb.de/fractalqb/xsx"`

---

Package XSX provides tools for parsing something I call eXtended S-eXpressions.
Extended means the following things compared to [SEXP
S-expressions](https://people.csail.mit.edu/rivest/sexp.html):

1. Nested structures are delimited by a configurable set of balanced brackets
   e.g., '()', '[]' or '{}’ – not only by '()'.

2. XSX provides a notation for "Meta Values", i.e. XSXs that provide some sort
   of out-of-band information.

On the other hand some properties from SEXP were dropped, e.g. typing of the so
called "octet strings". Things like that are completely left to the application.

## Somewhat more formal description

Frist of all, XSX is not about datatypes, in this it is comparable to e.g. XML
(No! don't leave… its much simpler). Instead, its building block is the _atom_
i.e., nothing else than a sequence of characters, aka a 'string'. Atoms come as
_quoted atoms_ and as _unquoted atoms_. An atom has to be quoted when the atom's
string contains characters that have a special meaning in XSX: White-space,
brackets, the quote and the meta character.

### Regexp style definition of Atom
    
    ws           := UNICODE whitespace
    o-brackets   := “set of opening brackets” – e.g. '(' | '[' | '{'
    c-brackets   := “set of closing brackets” – e.g. ')' | ']' | '}'
    quote        := “the quote char”          – e.g. "
    meta         := “the meta char”           – e.g. \
    syntax-chars := o-brackets | c-brackets | quote | meta

    atom     := nq-atom | q-atom
    nq-atom  := (^(syntax-chars|ws))+
	q-atom   := quote(quote{2}|^quote)*quote

I.e. `x` is an atom and `foo`, `bar` and `baz` are atoms. An atom that contains
space, a '"' or '\\' would be quoted. Also `"("` is an atom but `(` is not an
atom.

### Sequences now BNF Style

Each atom is an XSX and from XSX'es one can build sequences:

    XSX      := atom | sequence
    sequence := o-bracket {ws} c-bracket | o-bracket {ws} xsxs {ws} c-bracket
    xsxs     := XSX | XSX {ws} xsxs

### Out-Of-Band Information with Meta XSXs

You can prefix each XSX with the meta char to make that expression a
meta-expression. A meta-expression is not considered to be an XSX, i.e. you
cannot create meta-meta-expressions or meta-meta-meta-expressions… hmm… and not
even meta-meta-meta-meta-expressions! I think it became clear?

The special case when the meta char does not prefix an XSX e.g., “`\ `” or
`(\)`, makes the meta char the special meta token `void`.  One might perhaps use
it to denote tha absence of something.

E.g. `\4711` is a meta-atom and `\{foo 1 bar false baz 3.1415}` is a
meta-sequence. What _meta_ means is completely up to the
application. Imagine `(div hiho)` and `(div \{class green} hiho)`
to be a translation from `<div>hiho</div>` and `<div
class="green">hiho</div>`.

## Rationale

None! … despite the fact that I found it to be fun – and useful in
some situations. Because XSX syntax so simple it is easy to use for proprietary data files.

The first implementation was inspired by the
[expat](https://libexpat.github.io/) streaming parser that allows one to push
some data into the paring machinery and it will fire appropriate callbacks when
tokes are detected. Reimplementing it as a simple pulling scanner simplified the
code dramatically. With Go routines one can easily turn that into the streaming
and event driven model again. 

So, if you are looking for something that's even simpler than JSON or
YAML you might give it a try… Happy coding!
