# Extended S-Expression Objects

\(\# (1 2 3) \= d) "data"
\(\# (1 2 3) \d) "data"
\{# (1 2 3) = d} "data"

\(\# foo \= d) "data"
\{# foo = d) "data"
\(\#foo \d) "data"

Principles for object serialization with XSX:

- Put only pure data elements into XSX

- Anything else is meta data, e.g.:

    - names of sequence elements
	
    - IDs
	
    - References
	
- A meta value refers to the next …???… non-meta XSX

=> One can have many meta values for a non-meta w/o putting them
   all into one sequence!

## Standard Meta Attributes

_**TODO:** Have a Meta-Key that applies to a non-meta value → howto
find that non-meta value…_

With this we define the following types of meta attributes:

- **Names:** The `meta-key` `\=` makes the `meta-val` a name. Names
  are unique identifiers within their next containing non-meta XSX
  sequence, i.e. `\(\= foo) "'bar' is the second metasyntactical
  variable"` expresses the fact that foo the name for the string
  "'bar' is the…". _Note:_ The name ':' and names starting with '#'
  have a special meaning!

- **Keys:** The `meta-key` `\#`_domain_ makes the `meta-val` a unique
  key with in a specific _domain_. E.g. `\#mem 0x934fa398` would make
  `0x934fa398` a unique key in the 'mem' domain which—by reasonable
  convection—could be the memory address of some object. The name of
  the _default domain_ is the empty string, i.e.  `\(\#
  91e17810-af7b-4fbf-a6fb-bab2ff3b8c39) "'bar' is the…"` would assign
  a UUID to the string `"'bar' is the…"`.

- **Types:** The `meta-key` `\:` makes the `meta-val` a type tag. So
  that `\(\: float32) 4711` tells us that 4711 is meant to be a 32-bit
  float value.

**Why have such a complicated notation?**

The notation was chosen so that one can easily add other application
specific meta attributes to XSX expressions. E.g. a PGP signed
database record with id 2345 of type customer could be:

```
\(\: customer \#DB 2345 \pgp [
    iQGzBAABCgAdFiEE5NvqYl7Ph2GngBGqZE2C5RBlzqoFAlxZgmAACgkQZE2C5RBl
…
    =kPYT])
  [\(\= surname) Doe
   \(\= called) John
   \(\= street) "Yellow Brick Road"
   \(\= country) Oz
   \(\= tax-id \#tax) 765432]
```

this still looks a little bit clumsy. Therefore we need…

## Some abbreviation rules

Abbreviations can be applied when there is only one Meta Attribute.

### Abbreviate Names

When an XSX expression only has one name meta attribute `\= foo` this
can be abbreviated as `\foo`. This makes the John Doe example somewhat
more readable:

```
\(\: customer \#DB 2345 \pgp [
    iQGzBAABCgAdFiEE5NvqYl7Ph2GngBGqZE2C5RBlzqoFAlxZgmAACgkQZE2C5RBl
…
    =kPYT])
  [\surname Doe
   \called John
   \street "Yellow Brick Road"
   \country Oz
   \(\= tax-id \#tax) 765432]
```

That's not the only interesting fact about name abbreviations. Now we
also see that ':' is the name of a type meta attribute so that `\:
customer` denotes the type 'customer'. An also names starting with '#'
denote keys from some key-domain.

### Record Sequences

By definition XSX sequences delimited by curly braces '{}' are used as
records, i.e. each odd element is implicitly a meta XSX and does not
have to start with '\'. Each even element of a record is a non-meta
XSX.

With this abbreviation rules the John Doe example finally becomes:

```
\{: customer #DB 2345 pgp [
    iQGzBAABCgAdFiEE5NvqYl7Ph2GngBGqZE2C5RBlzqoFAlxZgmAACgkQZE2C5RBl
…
    =kPYT]}
  {surname Doe
   called John
   street "Yellow Brick Road"
   country Oz
   {= tax-id # tax} 765432}
```

## Convention about () / []
Use [] for non-meta values and use () for meta values

TODO: Decide is () is meta only and abbrev rules of {} apply
