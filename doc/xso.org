#+TITLE: Extended S-Expression Objects

- Put only pure data elements into XSX
- Anything else is meta data
  - names of sequence elements
  - IDs
  - References
- A meta value refers to the next following XSX

* Standard Meta Attributes
- Names :: \= street (= local key)
- Address :: \&___ 4711
- Is Key :: \# setname  (_ = empty setname)
- Type :: \: ___

** Some standard meta atts have abbrevs
Abbreviations can be applied when there is only one Meta Att

- Names :: \street
- Id Key :: \#setname

\(\= street) ... → \street ...
\(\# set) ... → \#set
\(\# _) ... → \# ...

* Records '{}' have metas
In a {}-sequence every odd element is implicitly meta and the
{}-sequence must have an event number of elements

\(\&DB 12345) (\name "John Doe" \dob 1980-10-11 \(\= tax-id \# _) 3485643)

becomes

\{&DB 12345} {name "John Doe"
              dob 1980-10-11 
              {= tax-id # _} 3485643}

* Konvention about () / []
Use [] for non-meta values and use () for meta values

TODO: Decide is () is meta only and abbrev rules of {} apply
