package xsx

import (
	"errors"
	"fmt"
	"strings"
)

type ExpectModifier int

const (
	IsMeta ExpectModifier = (1 << iota)
	NotMeta
	IsQuote
	NotQuote
)

type Fails []error

func (err Fails) Error() string {
	if len(err) == 0 {
		return ""
	}
	var sb strings.Builder
	sb.WriteByte('[')
	sb.WriteString(err[0].Error())
	for _, f := range err[1:] {
		sb.WriteString("] & [")
		sb.WriteString(f.Error())
	}
	sb.WriteByte(']')
	return sb.String()
}

type Expectation struct {
	tok    *Token
	checks int
	fails  Fails
}

func Expect(t *Token) *Expectation {
	return &Expectation{tok: t}
}

func (ex *Expectation) Failed() bool { return len(ex.fails) >= ex.checks }

func (ex *Expectation) Fails() Fails { return ex.fails }

func (ex *Expectation) Reset(t *Token) *Expectation {
	if t != nil {
		ex.tok = t
	}
	ex.checks = 0
	ex.fails = nil
	return ex
}

func (ex *Expectation) Space() bool {
	if ex.tok.Type != Space {
		ex.checks++
		ex.fails = append(ex.fails, fmt.Errorf("token is %s, not Space", ex.tok.Type))
		return false
	}
	return true
}

func (ex *Expectation) AnyAtom(mod ExpectModifier) bool {
	ex.checks++
	if ex.tok.Type != Atom {
		ex.fails = append(ex.fails, fmt.Errorf("token is %s, not Atom", ex.tok.Type))
		return false
	}
	if mod&IsMeta != 0 && !ex.tok.Meta {
		ex.fails = append(ex.fails, errors.New("Atom is not meta"))
		return false
	}
	if mod&NotMeta != 0 && ex.tok.Meta {
		ex.fails = append(ex.fails, errors.New("Atom is meta"))
		return false
	}
	if mod&IsQuote != 0 && !ex.tok.Quoted {
		ex.fails = append(ex.fails, errors.New("Atom is not quoted"))
		return false
	}
	if mod&NotQuote != 0 && ex.tok.Quoted {
		ex.fails = append(ex.fails, errors.New("Atom is quoted"))
		return false
	}
	return true
}

func (ex *Expectation) Atom(s string, mod ExpectModifier) bool {
	ex.checks++
	if ex.tok.Type != Atom {
		ex.fails = append(ex.fails, fmt.Errorf("token is %s, not Atom", ex.tok.Type))
		return false
	}
	if ts := ex.tok.String(); ts != s {
		ex.fails = append(ex.fails, fmt.Errorf("expect Atom '%s', got '%s'", s, ts))
		return false
	}
	if mod&IsMeta != 0 && !ex.tok.Meta {
		ex.fails = append(ex.fails, errors.New("Atom is not meta"))
		return false
	}
	if mod&NotMeta != 0 && ex.tok.Meta {
		ex.fails = append(ex.fails, errors.New("Atom is meta"))
		return false
	}
	if mod&IsQuote != 0 && !ex.tok.Quoted {
		ex.fails = append(ex.fails, errors.New("Atom is not quoted"))
		return false
	}
	if mod&NotQuote != 0 && ex.tok.Quoted {
		ex.fails = append(ex.fails, errors.New("Atom is quoted"))
		return false
	}
	return true
}

func (ex *Expectation) Begin(mod ExpectModifier, bs ...rune) bool {
	ex.checks++
	if ex.tok.Type != Begin {
		ex.fails = append(ex.fails, fmt.Errorf("token is %s, not Begin", ex.tok.Type))
		return false
	}
	if len(bs) > 0 {
		tb := ex.tok.Rune()
		for _, b := range bs {
			if b == tb {
				goto GOOD_BRACKET
			}
		}
		var good strings.Builder
		for i, b := range bs {
			if i > 0 {
				good.WriteString(", ")
			}
			fmt.Fprintf(&good, "'%c'", b)
		}
		ex.fails = append(ex.fails, fmt.Errorf(
			"unexpected Begin bracket '%c', expecting on of %s",
			tb,
			good.String(),
		))
		return false
	}
GOOD_BRACKET:
	if mod&IsMeta != 0 && !ex.tok.Meta {
		ex.fails = append(ex.fails, errors.New("Begin is not meta"))
		return false
	}
	if mod&NotMeta != 0 && ex.tok.Meta {
		ex.fails = append(ex.fails, errors.New("Begin is meta"))
		return false
	}
	return true
}

func (ex *Expectation) End() bool {
	if ex.tok.Type != End {
		ex.checks++
		ex.fails = append(ex.fails, fmt.Errorf("token is %s, not End", ex.tok.Type))
		return false
	}
	return true
}

func (ex *Expectation) Void() bool {
	if ex.tok.Type != Void {
		ex.checks++
		ex.fails = append(ex.fails, fmt.Errorf("token is %s, not Void", ex.tok.Type))
		return false
	}
	return true
}
