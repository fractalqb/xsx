package xsx

import "fmt"

func ExampleExpectation() {
	var tok Token
	scn := NewStringDefaultScanner("\\")
	if err := scn.Next(&tok, false); err != nil {
		fmt.Println(err)
		return
	}
	ex := *Expect(&tok)
	switch {
	case ex.Begin(0, '('):
		fmt.Println("begin (")
	case ex.Atom("foo", IsQuote|NotMeta):
		fmt.Println("found a \"foo\"")
	default:
		fmt.Println(ex.Fails())
	}
	fmt.Println("Expectation failed:", ex.Failed())
	// Output:
	// [token is Void, not Begin] & [token is Void, not Atom]
	// Expectation failed: true
}
