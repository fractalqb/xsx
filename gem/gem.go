// Package gem provides a GEneric Model for XSX data
package gem

import (
	"errors"

	"git.fractalqb.de/fractalqb/xsx"
)

type XSX interface {
	Meta() bool
	SetMeta(bool)
	Write(*xsx.Writer) (int, error)
}

type xsxBase bool

func (b xsxBase) Meta() bool { return bool(b) }

func (b *xsxBase) SetMeta(meta bool) {
	*b = xsxBase(meta)
}

var Void void

type Atom struct {
	xsxBase
	Text string
}

func (a *Atom) Write(w *xsx.Writer) (n int, err error) {
	_, n, err = w.Atom(a.Text, a.Meta())
	return
}

type Sequence struct {
	xsxBase
	Bracket int
	Elem    []XSX
}

func (s *Sequence) Write(w *xsx.Writer) (n int, err error) {
	if n, err = w.Begin(s.Bracket, s.Meta()); err != nil {
		return n, err
	}
	for _, e := range s.Elem {
		m, err := e.Write(w)
		if err != nil {
			return n + m, err
		}
		n += m
	}
	m, err := w.End()
	return m + n, err
}

func Parse(s *xsx.Scanner) (res XSX, err error) {
	var (
		tok   xsx.Token
		seq   *Sequence
		stack []*Sequence
	)
	for err = s.Next(&tok, false); err == nil; err = s.Next(&tok, false) {
		switch tok.Type {
		case xsx.Atom:
			a := &Atom{xsxBase: xsxBase(tok.Meta), Text: tok.String()}
			if seq == nil {
				return a, nil
			}
			seq.Elem = append(seq.Elem, a)
		case xsx.Begin:
			s := &Sequence{
				xsxBase: xsxBase(tok.Meta),
				Bracket: s.Syntax().IsBegin(tok.Rune()),
			}
			if seq != nil {
				seq.Elem = append(seq.Elem, s)
				stack = append(stack, seq)
			}
			seq = s
		case xsx.End:
			l := len(stack) - 1
			if l < 0 {
				return seq, nil
			}
			seq = stack[l]
			stack = stack[:l]
		case xsx.Void:
			if seq == nil {
				return Void, nil
			}
			seq.Elem = append(seq.Elem, Void)
		}
	}
	return nil, errors.New("incomplete data")
}

type void struct{}

func (void) Meta() bool { return true }

func (void) SetMeta(bool) {}

func (void) Write(w *xsx.Writer) (int, error) { return w.Void() }
