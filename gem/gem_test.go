package gem

import (
	"fmt"
	"os"
	"strings"

	"git.fractalqb.de/fractalqb/xsx"
)

func Example() {
	scn := xsx.NewDefaultScanner(strings.NewReader(
		`(foo \[bar] \baz)`,
	))
	gem, err := Parse(scn)
	if err != nil {
		fmt.Println(err)
	} else {
		wr := xsx.NewDefaultWriter(os.Stdout)
		gem.Write(wr)
		wr.Flush()
	}
	// Output:
	// (foo \[bar] \baz)
}
