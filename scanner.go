package xsx

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"strings"
	"unicode"
)

//go:generate stringer -type TokenType
type TokenType int

const (
	Space TokenType = iota + 1
	Atom
	Begin
	End
	Void
)

type Token struct {
	strings.Builder
	Type         TokenType
	Meta, Quoted bool
}

// Returns unicode.MaxRune+1 if no rune is available
func (t *Token) Rune() rune {
	s := t.String()
	if s == "" {
		return unicode.MaxRune + 1
	}
	return ([]rune(s))[0]
}

type Scanner struct {
	syn  Syntax
	rd   *bufio.Reader
	stat []int
}

func NewScanner(r io.Reader, s Syntax) (scn *Scanner, err error) {
	if err = s.validate(); err != nil {
		return nil, err
	}
	scn = &Scanner{syn: s}
	var ok bool
	if scn.rd, ok = r.(*bufio.Reader); !ok {
		scn.rd = bufio.NewReader(r)
	}
	return scn, nil
}

func NewDefaultScanner(r io.Reader) *Scanner {
	scn := &Scanner{syn: DefaultSyntax()}
	var ok bool
	if scn.rd, ok = r.(*bufio.Reader); !ok {
		scn.rd = bufio.NewReader(r)
	}
	return scn
}

func NewStringScanner(str string, s Syntax) (*Scanner, error) {
	return NewScanner(strings.NewReader(str), s)
}

func NewStringDefaultScanner(str string) *Scanner {
	return NewDefaultScanner(strings.NewReader(str))
}

func (scn *Scanner) Syntax() Syntax { return scn.syn }

func (scn *Scanner) MustNext(tok *Token, space bool) bool {
	err := scn.Next(tok, space)
	switch {
	case err == io.EOF:
		return false
	case err != nil:
		panic(err)
	}
	return true
}

func (scn *Scanner) Next(tok *Token, space bool) error {
	tok.Reset()
	r, _, err := scn.rd.ReadRune()
	if err != nil {
		return err
	}
	if !space && unicode.IsSpace(r) {
		if r, err = scn.skipSpace(); err != nil {
			return err
		}
	}
	if r == scn.syn.Meta() {
		tok.Meta = true
		if r, _, err = scn.rd.ReadRune(); err == io.EOF {
			tok.Type = Void
			tok.Quoted = false
			return nil
		} else if err != nil {
			return err
		}
	} else {
		tok.Meta = false
	}
	tok.Quoted = false
	switch {
	case unicode.IsSpace(r):
		if tok.Meta {
			tok.Type = Void
			return scn.rd.UnreadByte()
		}
		tok.Type = Space
		tok.WriteRune(r)
		return scn.readSpace(&tok.Builder)
	case r == scn.syn.Quote():
		tok.Type = Atom
		tok.Quoted = true
		return scn.readQuoted(&tok.Builder)
	}
	if b := scn.syn.IsBegin(r); b >= 0 {
		tok.Type = Begin
		tok.Builder.WriteRune(r)
		scn.stat = append(scn.stat, b)
		return nil
	}
	if b := scn.syn.IsEnd(r); b >= 0 {
		if tok.Meta {
			tok.Type = Void
			return scn.rd.UnreadRune()
		}
		l := len(scn.stat)
		if l == 0 {
			return fmt.Errorf("unbalanced end token %c", r)
		}
		c := scn.stat[l-1]
		scn.stat = scn.stat[:l-1]
		if c != b {
			return fmt.Errorf("end token %c mismatches %c", r, scn.syn.toks[2+c])
		}
		tok.Type = End
		tok.Builder.WriteRune(r)
		return nil
	}
	tok.Type = Atom
	tok.Quoted = false
	tok.WriteRune(r)
	return scn.readUnquoted(&tok.Builder)
}

func (scn *Scanner) skipSpace() (rune, error) {
	for {
		r, _, err := scn.rd.ReadRune()
		if err != nil {
			return r, err
		}
		if !unicode.IsSpace(r) {
			return r, nil
		}
	}
}

func (scn *Scanner) readSpace(sb *strings.Builder) error {
	for {
		r, _, err := scn.rd.ReadRune()
		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return err
		}
		if !unicode.IsSpace(r) {
			return scn.rd.UnreadRune()
		}
		sb.WriteRune(r)
	}
}

func (scn *Scanner) readQuoted(sb *strings.Builder) error {
	for {
		r, _, err := scn.rd.ReadRune()
		switch {
		case err == io.EOF:
			return errors.New("unterminated quoted atom")
		case err != nil:
			return err
		}
		if err != nil {
			return err
		}
		if r == scn.syn.Quote() {
			r, _, err = scn.rd.ReadRune()
			switch {
			case err == io.EOF:
				return nil
			case err != nil:
				return err
			}
			if r != scn.syn.Quote() {
				return scn.rd.UnreadRune()
			}
		}
		sb.WriteRune(r)
	}
}

func (scn *Scanner) readUnquoted(sb *strings.Builder) error {
	for {
		r, _, err := scn.rd.ReadRune()
		switch {
		case err == io.EOF:
			return nil
		case err != nil:
			return err
		}
		if unicode.IsSpace(r) || scn.syn.IsToken(r) {
			scn.rd.UnreadRune()
			return nil
		}
		sb.WriteRune(r)
	}
}
