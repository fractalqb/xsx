package xsx

import (
	"fmt"
	"io"
	"strings"
	"testing"
	"unicode"
)

func Example() {
	p := NewDefaultScanner(strings.NewReader(` "xyz"foo `))
	var tok Token
	var err error
	for err = p.Next(&tok, true); err == nil; err = p.Next(&tok, true) {
		fmt.Printf("%s [%s] %t %t\n", tok.Type, tok.String(), tok.Meta, tok.Quoted)
	}
	fmt.Println(err)
	// Output:
	// Space [ ] false false
	// Atom [xyz] false true
	// Atom [foo] false false
	// Space [ ] false false
	// EOF
}

func TestToken_Rune(t *testing.T) {
	var tok Token
	if r := tok.Rune(); r <= unicode.MaxRune {
		t.Errorf("empty token returns rune '%c'", r)
	}
	tok.WriteString("foo")
	if r := tok.Rune(); r != 'f' {
		t.Errorf("empty token returns unexpected rune '%c'", r)
	}
}

func TestNewScanner(t *testing.T) {
	t.Run("empty syntax", func(t *testing.T) {
		var syn Syntax
		_, err := NewStringScanner("", syn)
		switch {
		case err == nil:
			t.Error("empty syntax not detected")
		}
	})
	t.Run("happy case", func(t *testing.T) {
		syn, err := NewSyntax("(", ")", '"', '\\')
		if err != nil {
			t.Fatal(err)
		}
		_, err = NewStringScanner(`(x "y" z)`, syn)
		if err != nil {
			t.Fatal(err)
		}
		// TODO check something else?
	})
}

type tokens struct {
	t *testing.T
	p *Scanner
}

func (tks *tokens) expect(typ TokenType, txt string, meta, quote bool) {
	var tok Token
	if err := tks.p.Next(&tok, true); err != nil {
		tks.t.Fatal(err)
	}
	if tok.Type != typ {
		tks.t.Error("wrong token type", tok.Type)
	}
	if tok.Meta != meta {
		tks.t.Error("want meta", meta, "got", tok.Meta)
	}
	if tok.Quoted != quote {
		tks.t.Error("want quote", quote, "got", tok.Quoted)
	}
	if s := tok.String(); s != txt {
		tks.t.Errorf("want text '%s' got '%s'", txt, s)
	}
}

func (tks *tokens) eof() {
	var tok Token
	err := tks.p.Next(&tok, true)
	if err != io.EOF {
		tks.t.Error(err)
	}
}

func TestParser_tokens(t *testing.T) {
	t.Run("space", func(t *testing.T) {
		p := NewStringDefaultScanner(" ")
		toks := tokens{t, p}
		toks.expect(Space, " ", false, false)
		toks.eof()
	})
	t.Run("()", func(t *testing.T) {
		p := NewStringDefaultScanner("()")
		toks := tokens{t, p}
		toks.expect(Begin, "(", false, false)
		toks.expect(End, ")", false, false)
		toks.eof()
	})
	t.Run("[]", func(t *testing.T) {
		p := NewStringDefaultScanner("[]")
		toks := tokens{t, p}
		toks.expect(Begin, "[", false, false)
		toks.expect(End, "]", false, false)
		toks.eof()
	})
	t.Run("{}", func(t *testing.T) {
		p := NewStringDefaultScanner("{}")
		toks := tokens{t, p}
		toks.expect(Begin, "{", false, false)
		toks.expect(End, "}", false, false)
		toks.eof()
	})
	t.Run("meta begin", func(t *testing.T) {
		p := NewStringDefaultScanner(`\[`)
		toks := tokens{t, p}
		toks.expect(Begin, "[", true, false)
		toks.eof()
	})
	t.Run("unquoted", func(t *testing.T) {
		p := NewStringDefaultScanner("foo")
		toks := tokens{t, p}
		toks.expect(Atom, "foo", false, false)
		toks.eof()
	})
	t.Run("quoted", func(t *testing.T) {
		p := NewStringDefaultScanner("\"foo bar\"")
		toks := tokens{t, p}
		toks.expect(Atom, "foo bar", false, true)
		toks.eof()
	})
	t.Run("empty", func(t *testing.T) {
		p := NewStringDefaultScanner(`""`)
		toks := tokens{t, p}
		toks.expect(Atom, "", false, true)
		toks.eof()
	})
	t.Run("meta empty", func(t *testing.T) {
		p := NewStringDefaultScanner(`\""`)
		toks := tokens{t, p}
		toks.expect(Atom, "", true, true)
		toks.eof()
	})
	t.Run("void", func(t *testing.T) {
		p := NewStringDefaultScanner(`\`)
		toks := tokens{t, p}
		toks.expect(Void, "", true, false)
		toks.eof()
	})
	t.Run("void space", func(t *testing.T) {
		p := NewStringDefaultScanner(`\ `)
		toks := tokens{t, p}
		toks.expect(Void, "", true, false)
		toks.expect(Space, " ", false, false)
		toks.eof()
	})
	t.Run("void element", func(t *testing.T) {
		p := NewStringDefaultScanner(`(\)`)
		toks := tokens{t, p}
		toks.expect(Begin, "(", false, false)
		toks.expect(Void, "", true, false)
		toks.expect(End, ")", false, false)
		toks.eof()
	})
}

func TestToken_quoting(t *testing.T) {
	scn := NewStringDefaultScanner(`"not terminated`)
	var tok Token
	err := scn.Next(&tok, false)
	if err == nil {
		t.Fatal("unterminated quoted atom not detected")
	}
	if err.Error() != "unterminated quoted atom" {
		t.Error("unexpected error:", err)
	}
}
