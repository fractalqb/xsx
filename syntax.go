package xsx

import (
	"errors"
	"fmt"
	"strings"
	"unicode"
)

type Syntax struct {
	toks     []rune
	brackets int
}

func NewSyntax(begin, end string, quote, meta rune) (Syntax, error) {
	syn := Syntax{toks: []rune{quote, meta}}
	b := []rune(begin)
	syn.brackets = len(b)
	syn.toks = append(syn.toks, b...)
	syn.toks = append(syn.toks, []rune(end)...)
	if err := syn.validate(); err != nil {
		return Syntax{}, err
	}
	return syn, nil
}

func DefaultSyntax() Syntax {
	return Syntax{
		toks:     []rune{'"', '\\', '(', '[', '{', ')', ']', '}'},
		brackets: 3,
	}
}

func (s Syntax) Quote() rune { return s.toks[0] }

func (s Syntax) Meta() rune { return s.toks[1] }

func (s Syntax) Begin() []rune { return s.toks[2 : 2+s.brackets] }

func (s Syntax) End() []rune { return s.toks[2+s.brackets:] }

func (s Syntax) IsBegin(r rune) int {
	for i, b := range s.Begin() {
		if b == r {
			return i
		}
	}
	return -1
}

func (s Syntax) IsEnd(r rune) int {
	for i, b := range s.End() {
		if b == r {
			return i
		}
	}
	return -1
}

func (s Syntax) IsToken(r rune) bool {
	for _, t := range s.toks {
		if t == r {
			return true
		}
	}
	return false
}

func (syn Syntax) QuoteIf(s string) (string, bool) {
	var sb strings.Builder
	var quoted bool
	sb.WriteRune(syn.Quote())
	if s == "" {
		sb.WriteRune(syn.Quote())
		return sb.String(), true
	}
	for _, r := range s {
		switch {
		case r == syn.Quote():
			quoted = true
			sb.WriteRune(r)
		case unicode.IsSpace(r) || syn.IsToken(r):
			quoted = true
		}
		sb.WriteRune(r)
	}
	if quoted {
		sb.WriteRune(syn.Quote())
		return sb.String(), true
	}
	return s, false
}

func (s Syntax) validate() error {
	if s.brackets == 0 {
		return errors.New("no begin runes")
	}
	if len(s.toks) != 2+2*s.brackets {
		return fmt.Errorf("number of begin and end runes differ")
	}
	for _, b := range s.Begin() {
		if b == s.Meta() {
			return fmt.Errorf("rune '%c' is begin and meta", b)
		}
		if b == s.Quote() {
			return fmt.Errorf("rune '%c' is begin and quote", b)
		}
		for _, e := range s.End() {
			if e == s.Meta() {
				return fmt.Errorf("rune '%c' is end and meta", e)
			}
			if e == s.Quote() {
				return fmt.Errorf("rune '%c' is end and quote", e)
			}
			if e == b {
				return fmt.Errorf("rune '%c' is begin and end", e)
			}
		}
	}
	return nil
}
