package xsx

import (
	"fmt"
	"testing"
)

func TestDefaultSyntax(t *testing.T) {
	syn := DefaultSyntax()
	if err := syn.validate(); err != nil {
		t.Fatal(err)
	}
}

func ExampleSyntax_QuoteIf() {
	syn := DefaultSyntax()
	fmt.Println(syn.QuoteIf(""))
	fmt.Println(syn.QuoteIf("foo"))
	fmt.Println(syn.QuoteIf("foo bar"))
	fmt.Println(syn.QuoteIf("(foo)"))
	fmt.Println(syn.QuoteIf("\\foo"))
	fmt.Println(syn.QuoteIf(`a "foo"`))
	// Output:
	// "" true
	// foo false
	// "foo bar" true
	// "(foo)" true
	// "\foo" true
	// "a ""foo""" true
}
