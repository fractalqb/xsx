package table

import (
	"errors"
	"fmt"

	"git.fractalqb.de/fractalqb/xsx"
	"git.fractalqb.de/fractalqb/xsx/gem"
)

type Column struct {
	Name string
	Meta bool
}

type Header []Column

func ReadHeader(s *xsx.Scanner) (h Header, err error) {
	defer func() {
		if p := recover(); p != nil {
			h = nil
			err = asError(p)
		}
	}()
	var tok xsx.Token
	if !s.MustNext(&tok, false) {
		return nil, nil
	}
	ex := *xsx.Expect(&tok)
	if !ex.Begin(xsx.NotMeta, '[') {
		return nil, ex.Fails()
	}
	for s.MustNext(&tok, false) {
		ex.Reset(nil)
		switch {
		case tok.Type == xsx.Space:
		case ex.AnyAtom(0):
			h = append(h, Column{Name: tok.String(), Meta: tok.Meta})
		case ex.End():
			return h, nil
		default:
			return nil, ex.Fails()
		}
	}
	return nil, errors.New("incomplete header")
}

func (hdr Header) ReadRow(s *xsx.Scanner) (row []gem.XSX, err error) {
	defer func() {
		if p := recover(); p != nil {
			row = nil
			err = asError(p)
		}
	}()
	var tok xsx.Token
	if err = s.Next(&tok, false); err != nil {
		return nil, err
	}
	ex := *xsx.Expect(&tok)
	if !ex.Begin(xsx.NotMeta, '(') {
		return nil, ex.Fails()
	}
	for i := range hdr {
		xsx, err := gem.Parse(s)
		if err != nil {
			return row, fmt.Errorf("column %d: %w", i, err)
		}
		row = append(row, xsx)
	}
	if !s.MustNext(&tok, false) {
		return row, errors.New("incomplete row")
	}
	if !ex.Reset(&tok).End() {
		return row, ex.Fails()
	}
	return row, nil
}

func asError(p any) error {
	if p != nil {
		switch p := p.(type) {
		case error:
			return p
		case string:
			return errors.New(p)
		default:
			return fmt.Errorf("panic: %+v", p)
		}
	}
	return nil
}
