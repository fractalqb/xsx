package table

import (
	"fmt"
	"os"

	"git.fractalqb.de/fractalqb/xsx"
	"git.fractalqb.de/fractalqb/xsx/gem"
)

func ExampleHeader() {
	scn := xsx.NewStringDefaultScanner(`
[foo \bar]
(1 \)
("next is a list" [1 2 3])`)
	hdr, err := ReadHeader(scn)
	fmt.Println(err, hdr)
	var row []gem.XSX
	for row, err = hdr.ReadRow(scn); err == nil; row, err = hdr.ReadRow(scn) {
		rw, _ := xsx.NewWriter(os.Stdout, scn.Syntax())
		for _, e := range row {
			e.Write(rw)
		}
		rw.Flush()
		fmt.Fprintln(os.Stdout)
	}
	fmt.Println(err)
	// Output:
	// <nil> [{foo false} {bar true}]
	// 1 \
	// "next is a list" [1 2 3]
	// EOF
}
