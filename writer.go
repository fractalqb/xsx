package xsx

import (
	"bufio"
	"fmt"
	"io"
	"unicode"
)

type Writer struct {
	syn  Syntax
	wr   *bufio.Writer
	stat []rune
	spc  bool
}

func NewWriter(w io.Writer, s Syntax) (wr *Writer, err error) {
	if err = s.validate(); err != nil {
		return nil, err
	}
	wr = &Writer{syn: s}
	var ok bool
	if wr.wr, ok = w.(*bufio.Writer); !ok {
		wr.wr = bufio.NewWriter(w)
	}
	return wr, nil
}

func NewDefaultWriter(w io.Writer) *Writer {
	wr := &Writer{syn: DefaultSyntax()}
	var ok bool
	if wr.wr, ok = w.(*bufio.Writer); !ok {
		wr.wr = bufio.NewWriter(w)
	}
	return wr
}

func (w *Writer) Flush() error { return w.wr.Flush() }

func (w *Writer) Begin(b int, meta bool) (n int, err error) {
	if n, err = w.wspc(false); err != nil {
		return n, err
	}
	if meta {
		m, err := w.wr.WriteRune(w.syn.Meta())
		if err != nil {
			return n + m, err
		}
		n += m
	}
	w.stat = append(w.stat, w.syn.End()[b])
	m, err := w.wr.WriteRune(w.syn.Begin()[b])
	return n + m, err
}

func (w *Writer) End() (int, error) {
	w.spc = true
	l := len(w.stat) - 1
	r := w.stat[l]
	w.stat = w.stat[:l]
	return w.wr.WriteRune(r)
}

func (w *Writer) Atom(a string, meta bool) (quoted bool, n int, err error) {
	if n, err = w.wspc(true); err != nil {
		return false, n, err
	}
	if meta {
		if m, err := w.wr.WriteRune(w.syn.Meta()); err != nil {
			return false, n + m, err
		} else {
			n += m
		}
	}
	a, quoted = w.syn.QuoteIf(a)
	m, err := w.wr.WriteString(a)
	return quoted, n + m, err
}

func (w *Writer) Void() (n int, err error) {
	if n, err = w.wspc(true); err != nil {
		return n, err
	}
	m, err := w.wr.WriteRune(w.syn.Meta())
	return n + m, err
}

func (w *Writer) Space(spc string) (n int, err error) {
	w.spc = false
	for _, r := range spc {
		if !unicode.IsSpace(r) {
			return n, fmt.Errorf("non-space rune '%c'", r)
		}
		m, err := w.wr.WriteRune(r)
		if err != nil {
			return m + n, err
		}
		n += m
	}
	return n, nil
}

func (w *Writer) wspc(spc bool) (n int, err error) {
	if w.spc {
		err = w.wr.WriteByte(' ')
		n = 1
	}
	w.spc = spc
	return
}
