package xsx

import "os"

func ExampleWriter() {
	syn, _ := NewSyntax("<", ">", '\'', '^')
	w, _ := NewWriter(os.Stdout, syn)
	w.Begin(0, false)
	w.Space("\n\t")
	w.Atom("foo 'n' bar", true)
	w.Space("\n\t")
	w.Atom("4711", false)
	w.Space("\n")
	w.End()
	w.Flush()
	// Output:
	// <
	// 	^'foo ''n'' bar'
	// 	4711
	// >
}

func ExampleWrite_spaces() {
	w := NewDefaultWriter(os.Stdout)
	w.Begin(0, false)
	w.Void()
	w.Atom("foo", false)
	w.End()
	w.Begin(0, false)
	w.Atom("bar", true)
	w.Void()
	w.End()
	w.Flush()
	// Output:
	// (\ foo) (\bar \)
}
